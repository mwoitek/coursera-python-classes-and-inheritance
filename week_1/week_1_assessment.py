# Problem 1


class Bike:
    def __init__(self, color, price):
        self.color = color
        self.price = price


testOne = Bike(color="blue", price=89.99)
testTwo = Bike(color="purple", price=25.0)


# Problem 2


class AppleBasket:
    def __init__(self, apple_color, apple_quantity):
        self.apple_color = apple_color
        self.apple_quantity = apple_quantity

    def __str__(self):
        return f"A basket of {self.apple_quantity} {self.apple_color} apples."

    def increase(self):
        self.apple_quantity += 1


basket1 = AppleBasket(apple_color="red", apple_quantity=4)
print(basket1)

basket2 = AppleBasket(apple_color="blue", apple_quantity=49)
basket2.increase()
print(basket2)


# Problem 3


class BankAccount:
    def __init__(self, name, amt):
        self.name = name
        self.amt = amt

    def __str__(self):
        return f"Your account, {self.name}, has {self.amt} dollars."


t1 = BankAccount(name="Bob", amt=100)
print(t1)
