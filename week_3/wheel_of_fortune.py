import random

LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
VOWELS = "AEIOU"
VOWEL_COST = 250


# Write the WOFPlayer class definition (part A) here


class WOFPlayer:
    def __init__(self, name):
        self.name = name
        self.prizeMoney = 0
        self.prizes = []

    def __str__(self):
        return f"{self.name} (${self.prizeMoney})"

    def addMoney(self, amt):
        self.prizeMoney += amt

    def goBankrupt(self):
        self.prizeMoney = 0

    def addPrize(self, prize):
        self.prizes.append(prize)


# Write the WOFHumanPlayer class definition (part B) here


class WOFHumanPlayer(WOFPlayer):
    def getMove(self, category, obscuredPhrase, guessed):
        print(f"{self.name} has ${self.prizeMoney}\n")
        print(f"Category: {category}")
        print(f"Phrase:   {obscuredPhrase}")
        print(f"Guessed:  {guessed}\n")
        return input("Guess a letter, phrase, or type 'exit' or 'pass': ")


# Write the WOFComputerPlayer class definition (part C) here


class WOFComputerPlayer(WOFPlayer):
    SORTED_FREQUENCIES = "ZQXJKVBPYGFWMUCLDRHSNIOATE"

    def __init__(self, name, difficulty):
        super().__init__(name)
        self.difficulty = difficulty

    def smartCoinFlip(self):
        return random.randint(1, 10) <= self.difficulty

    def getPossibleLetters(self, guessed):
        letters_lst = [letter for letter in LETTERS if letter not in guessed]
        if self.prizeMoney < VOWEL_COST:
            letters_lst = [letter for letter in letters_lst if letter not in VOWELS]
        return letters_lst

    def getMove(self, category, obscuredPhrase, guessed):
        letters_lst = self.getPossibleLetters(guessed)
        if len(letters_lst) == 0:
            return "pass"
        if self.smartCoinFlip():
            return next(filter(lambda letter: letter in letters_lst, self.SORTED_FREQUENCIES))
        return random.choice(letters_lst)
