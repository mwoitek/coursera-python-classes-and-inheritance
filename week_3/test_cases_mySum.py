import unittest

from test_cases_provided_code import mySum


class TestMySum(unittest.TestCase):
    def test_empty(self):
        self.assertEqual(mySum([]), 0)

    def test_one_item(self):
        self.assertEqual(mySum([9]), 9)
        self.assertEqual(mySum([0]), 0)
        self.assertEqual(mySum([-3]), -3)

    def test_more_than_one_item(self):
        self.assertEqual(mySum([1, 2]), 3)
        self.assertEqual(mySum([1, 2, -3]), 0)
        self.assertEqual(mySum([4, 4, 4, 4]), 16)


if __name__ == "__main__":
    unittest.main()
