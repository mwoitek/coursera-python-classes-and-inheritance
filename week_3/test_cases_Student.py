import unittest

from test_cases_provided_code import Student


class TestStudent(unittest.TestCase):
    def test_constructor_name(self):
        s = Student("Charles")
        self.assertEqual(s.name, "Charles")
        self.assertEqual(s.years_UM, 1)
        self.assertEqual(s.knowledge, 0)

    def test_constructor_name_years(self):
        s = Student("Chuck", 3)
        self.assertEqual(s.name, "Chuck")
        self.assertEqual(s.years_UM, 3)
        self.assertEqual(s.knowledge, 0)

    def test_study(self):
        s = Student("Charlie")
        self.assertIsNone(s.study())
        self.assertEqual(s.knowledge, 1)

    def test_getKnowledge(self):
        s = Student("James")
        self.assertEqual(s.getKnowledge(), 0)
        self.assertEqual(s.getKnowledge(), s.knowledge)

    def test_year_at_umich(self):
        s = Student("Jim")
        self.assertEqual(s.year_at_umich(), 1)
        self.assertEqual(s.year_at_umich(), s.years_UM)

        s = Student("Jimmy", 2)
        self.assertEqual(s.year_at_umich(), 2)
        self.assertEqual(s.year_at_umich(), s.years_UM)


if __name__ == "__main__":
    unittest.main()
